#import "lib.typ" : *

#let VS = [Vermittlungsschicht]

= Vermittlungsschicht
#definition[Paketvermittlung][
  Daten werden in Paketen vermittelt, die mit *Metainformationen* angereichert sind.
]

#fact[Pufferung][
  Die Vermittlungsschicht ist für Pufferung der Pakete verantwortlich!
  - Zwischensystem Puffern während dem weiterleiten, falls das Ausgangsinterface nicht verfügbar ist.
  - Verlust von Paketen möglich, falls die Pufferkapazität eines Zwischensystems (sprich: Router) überschritten wird
]

Die Vermittlungsschit hat 2 grundlegende Aufgaben:
/ Routing: Auf *Kontrollebene*
/ Forwarding: Auf *Datenebene* zwischen *Übertragungsabschnitten (Hops/Links)*

== Kontroll- & Datenebene
#concept[Kontrollebene][
/ Routingprotokoll: Oberhalb der Schicht #VS angesiedelt
/ Routingalgorithmus: Routingprotokolle setzen Routingalgorithmen um
/ Weiterleitungstabelle: #[
  - Enthält Einträge für mögliche Ziele mit Interfaces
  - Einträge werden von Routingprotokollen geliefert
  - Schnittstelle zwischen Kontroll- und Datenebene
]
]
#definition[Datenebene][
  #image("res/Kontroll vs Datenebene.png")
]

/ Router: Zwischensystem auf Vermittlungsschicht, das die Datagramme forwardet
/ Route:
/ Link: Übertragungsabschnitt zwischen zwei Routern
/ Port: Eingangs- oder Ausgangsinterface

== IP
#concept[IP-Adress][
  Eindeutige Identifikation von jedem Netzwerkinterface.\
]

#notation[IPv4/v6][
#set list(marker: zb)
  - 1080:0:0:0:7:700:100B:4711 = 1080::7:700:100B:4711\
  - 223.1.1.1 = 11011111 00000001 00000001 00000001\
  - 0:0:0:0 = ::1
  - #text(blue)[141.3.71.0]/#text(green)[24]\
    #text(blue)[Subnetz-Teil] und #text(green)[\#Bits in Subnet-Teil]
]

#definition[Subnetz][
  Interfaces im gleichen Subnetz können sich ohne Router erreichen.\
]

=== Internet Protocol (IP)
#concept[IP][
  - Best Effort
  - Verbindungslos
]
#fact[Internet Protocol][#image("res/Internet Protocol.png")]
#fact[Format eines IPv4-Datagramms][#image("res/IPv4 Datagramm.png")]

#definition[(Nur bei IPv4)][Fragmentierung][
  Aufspalten des Pakets zur Anpassung an verschieden lange maximale Rahmenlängen unterliegender Netze

  #definition[Maximum Transfer Unit (MTU)][
    Maximale Rahmenlänge (Fragmente werden auf diese Länge angepasst).
  ]
]

#concept[Forwarding][
  Weiterleiten an *Default-Router* oder diekt an Zieladresse, wenn im gleichen Subnetz
]

Bei Fehlern im Datagramm lesen wird das *Internet Control Message Protocol (ICMP)* aufgerufen.

== IPv6
- Keine Fragmentierung mehr
- Viele Vereinfachungen
- Viel größerer Adressraum

#align(center, image("res/IPv4 Header vs IPv6 Header.png", height: 30%))
/ Keep Alive: Gibt an, wieviele relay Schritte das Paket unterwegs sein darf

#fact[IPv6 Adressen][
Identifiziert...
  / Unicast: ein dediziertes Interface
  / Multicast: eine Gruppe von Interfaces (Datagramm geht an _alle_ Mitglieder)
  / Anycast: eine Gruppe von Interfaces (Datagramm geht an _ein_ Mitglied)
]

#definition[IPv6 Adresse][
  #image("res/IPv6 Adresse.png")
]

== Routing Verfahren
#definition[Klassifizierung][
- *Nicht adaptiv (statisch)* #vs *Adaptiv (dynamisch)*\
- *zentral* #vs *dezentral* (*isoliert* #vs *verteilt* (bezogen auf die Entscheidung, an wen es weiterleitet))\
]

#definition[Routing Verfahren][
/ Routingverfahren: Datagramme werden auf alle (außer dem, wo es hergekommen ist) Interfaces weitergeleitet. _(isoliert, statisch)_
/ Hot Potato Routing: Immer das Interface mit der kürzesten Warteschlange wählen. _(isoliert, adaptiv)_
/ Distanz-Vektor-Algorithmen: Routing-Metrik: Distanz _(verteilt, adaptiv)_
/ Link-States-Algorithmen: Jeder Router kennt die komplette Netztopologie... _(verteilt, adaptiv)_
]
=== Distanz-Vektor-Routing
#definition[Distanz-Vektor-Routing][
  - Verteilt: Jeder Router erhält Informationen von seinen direkten Nachbarn 
    und verteilt nach seiner Berechnung die neuen Informationen an seine Nachbarn.
  - Iterativ: Das Verteilen geht solange, bis keine neuen Informationen mehr ausgetauscht werden.
  Jeder Knoten verwaltet eine Tabelle, die für jeden Zielknoten und jeden Nachbar die optimale Distanz schätzt.
  Updates werden durch die Knoten propagiert.
]

#definition[Count-To-Infinity][
  Wenn eine Kante teuer werden, manche Knoten eine Route über diese Kante aber fälschlicherweise noch als Optimalen Weg gespeichert haben, ergeben sich
  zu lange Wege, die sich bis ins unendliche durch das Netzwerk ausbreiten.
]
-> *Poisened Reverse* als Lösung.

=== Link-State-Routing
#idea[Link-State-Routing][
+ Systeme kennen am Anfang nur ihre direkten Nachbarn
+ Bestimme Kosten zu direkten Nachbarn
  #idea[Link-State-Broadcasts][
    Identität und Kosten wird ins Netz geflutet (also an _alle_ Router)
  ]
]
#observe[
  - Da jeder Knoten identische Informationen hat sind alle berechneten kürzesten Pfade identisch
  - Nach Fluten konvergiert das System in einen schleifenfreien Zustand
]
#note[Link-State #vs Distanz-Vektor][
  - Link-State konvergiert schnell, ist aber kompliziert zu implementieren weil die Netztopologie rekonstruiert werden muss
  - Distanz-Vektor konvergiert langsam, ist aber einfach zu implmementieren
  -> Für kleine Netze Distanz-Vektor und für große Link-State
]

== Zuteilung von Adressen
#definition[Dynamic Host Configuration Protocol][
  Verteilt (zeitlich begrenzte) IP Adressen dynamisch auf Anfrage
  / DHCP Discover: Endsystem sendet Broadcast...
  / DHCP offer: ...DHCP Server antwortet mit
  / DHCP request: Endsystem fordert IP-Adresse an...
  / DHCP ack: ...DHCP Server teilt IP-Adresse zu
]

== Fehlerfälle
Einzeln Datagramm Verluste werden nicht gemeldet, weil IP ein unzuverlässiger Dienst ist. \
#definition[ICMP][
  Teilt den Schwerwiegende Probleme (#zb Unterbrechung einer Leitung) Kommunikationspartnern mit.\
  Unterstützt 
  - Fehlernachrichten
  - Statusanfragen
  - Zustandsinformationen
]
#definition[ICMP Statusanfragen][
  / Echo Nachricht: Empfänger sendet die Daten aus der Nachricht wieder zurück. Dient zur Statusanfrage
  / Timestamp: Datagramm umfasst mehrere Felder mit Zeitstempeln -> Bestimme Roundtrip time, Bearbeitungszeit und Propagation Time
]

#note[Zusammenfassung der Protokolle][
  - TCP
  - UDP
  - IP
  - ICMP
  - ARP & RARP (Zuordnung von IP-Adressen zu MAC-Adressen)
]

== Verbindungen
#definition[Virtuelle Verbindung][
Fester Pfad zwischen zwei Endsystemen \
-> Alle Pakete reihenfolgengetrue \
-> Mit Kontext/Verbindungsaufbau und Abbau
-> Alternative zu self-sufficient Datagrammen
]

#definition[Nachrichtenvermittlung][
  Nachrichten (aus Anwendersicht) werden mittels mehreren Paketen Hop-by-Hop zum Zielsystem weitergeleitet.
]
#definition[Circuit Switching][
  Verbindung wird durchgehender Übertragungskanal mit konstanter Datenrate zur Verfügung gestellt.
]

== Software-Defined Networking (SDN)
#definition[
  / Separierung: von Kontrollebene und Datenebene
  / Flow: -basierte Weiterleitung von Paketen
  / Controller: an den die Logik ausgelagert wird
  / Programmierbarkeit: des Netzwerks
]

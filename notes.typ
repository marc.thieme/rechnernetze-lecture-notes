#import "lib.typ" : *

#show: project.with("Einführung in Rechnernetze", "Zitterbart", "Marc Thieme")
#show strong: text.with(purple)

#set text(8pt)

= Klausuren
#definition(ws22(1))[Fluten][
  Fluten
]

= Einführung
== Rechnernetze
#definition[Begriffe][
  / Komponenten: Endzysteme oder Zwischensysteme
  / Links: Übertragungsmedien
  / Endsystem/Host: Führt verteilte Anwendungen aus
  / Zwischensystem: Leitet Daten
  / Lokales Netz (LAN):
  / Weitverkehrsnetz:
  #definition[Paket][
    Typischer Aufbau:
    - Nutzdaten
    - Metadaten
    Pakete werden vom Netz i.d.R. unabhängig voneinander behandelt
  ]
]

#observe[Probleme im Networking][
  - Layering (Schichtung)
  - Naming/Adressing
  - Routing (Wegwahl)
  - Reliability
  - Resource Sharing
]
/ Best Effort Service: Keine Zusicherung, ob Pakete ankmomen oder nicht
-> Netze sind in Schichten aufgebaut

#definition[5 Schichten][
  5. Anwendungsschicht (HTTP, SMTP)
  4. Transportschicht (UDP, TCP)
  3. Vermittlungsschicht (IPv4, IPv6)
  2. Sicherungsschicht (Ethernet, WLAN)
  1. Phyikalische Schicht (Ethernet, WLAN)
]

#note[Hourglass Modell][
  Hohe Diversität in Anwendungsschicht und Sicherungs-/Physikalischer Sicht. Nur
  Transportschicht (TCP) und Vermittlungsschicht (IP) bestehen aus wenigen
  Standarts, da sie einheitliche Kommunikation definieren müssen.
]

== Routing
Wegfindung für Pakete zum Ziel.
/ Sendewiederholung: durch Sender
/ Quittierungen: vom Emfpfänger
/ Timeout:
/ Sequenznummer: Um Paket zu identifizieren

#definition[Round Trip Time (RTT)][
  Die Zeit, die zwischen senden eines Pakets und empfangen der Antwort vergeht.
]
== Angriff
#definition[Bedrohung][
  Ein/mehrere Ereignisse, die zur Gefährdung einer oder mehrerer Schutzziele führt
]
#definition[Angriff][
  gezielte Realisierung einer Bedrohung
]
#definition[Angreifermodell][
  Fähigkeiten eines Angreifers
]
#definition[Dolev-Yao-Angreifer][
  + omnipräsent im Netz
  + Kann säntliche Kommunikation abhären
  + Eigene Pakete erzeugen und versenden
  + Fremde Pakete modifizieren
  Aber
  + Nicht entschlüsseln der verschlüsseln
  + Keinen Zugriff auf Endsysteme
]
#definition[Schutzziel][
  *Anforderung* an Komponente, um Güter vor Bedrohungen schützen
]
#example[
  - Vertrauligkeit
  - Integrity
  - Availability
  - Authentizität
  - Privatsphäre
  - (Nicht-)Abstreitbarkeit
]
#definition[Vertraulichkeit][
  Kein unautorisierter Informationsgewinn möglich
]
#definition[Schwache/Starke Integrität][
  Daten können nicht manipuliert werden $<-->$
  Manipulation können nicht _unbemerkt_ manipuliert werden
]

== Authentifizierung
#concept[MAC][
  / MAC: Message Authentication Code
  Einer Nachricht wird ihr Hash (basierend auf symmetrischem Schlüssel) angehängt.
  Nur, wer den Schlüssel besitzt, kann gültigen Hash generieren und die Nachricht
  gültig signieren.
]
#observe[
  Signatur sichert Integrität der Nachricht, authentifiziert aber nicht den
  Sender.
]
#concept[ID-Certificate][
  Bindet öffentlichen Schlüssel an Identität.\
  Wird von Certification Authority erstellt.
]

== Firewall
#definition[Zustandslose/Zustandsbehaftete Paketfilterung][
  Basierend auf z.B. IP Adressen, TCP/UDP Ports, Transportprotokll,
  Netzwerkinterface\
  $<==>$ Überwacht TCP Verbindung, Timeouts
]

== ISP
#definition[ISPs][
  / Access ISP: Anschluss von Endkunden, #zb 1&1
  / Regional ISP: #zb Vodafone, Tele2
  / Global ISP: #zb Deutsche Telekom AG, AT&T
  / Internet Exchange Point (IXP): "Treffpunkt" zum Datenaustausch
]
#image("res/ISP Net.png")

#definition[Adressarten][
  / IP: Netzwerkschicht
  / PORT: Transportschicht
  / MAC-Adresse: Sicherungsschicht
]
#fact[Protokolle][
  + HTTP: Anwendungsschicht
  + IP: Netzwerkschicht
  + TCP/UDP: Transportschicht
]

== Kommunikation
#observe[Fehlerquellen][
  *Bitefehler* $<==>$ Paketfehler
  + Bits verfälschen
  + Pakete gehen verloren
  + Pakete haben falsche Reihenfolge
  + Kommen doppelt an
  + Phantompaket
]

#definition[Zuverlässiger Dienst][
  + Alle empfangenen Daten sind _korrekt_
  + Alle gesendeten Daten werden _vollständig_ und in der _richtigen Reihenfolge_ empfangen
  + Es werden keine _Duplikate_ empfangen
  + Es werden keine _Phantomdaten_ empfangen
]

#concept[Protokoll für zuverlässigen Dienst][
  1. Sequenznummer für Pakete
  2. Acknowledgement (ACK) bei Empfang
    - Positive (ACK) und negative (NACK). #zb wenn eine Sequenznummer fehlt
    - Kann auch eine kumulative Quittung sein
]

=== Automatic Repeat Requests (ARQ)
Zur *Sendewiederholung* bei Paketfehlern
#todo[Stop and Wait][]
#todo[Alternating Bit Protocol (ABP)][]

== Leistungsbewertung
#concept[Metriken][
  / Parameter $a$: $a="Propagation Delay"/"Transmission Delay" = d/v dot r$ Länge des Mediums in
    Bit
  / Aulastung (Utilization): $"Transmission delay"/("Transmission Delay"+2"Propagation Delay")=1/(1+2a)$
  / Transmission Delay: $"# Bits"/"Datenrate"$" Zeit, um $n$ Bits zu senden. Hängt von Datenrate ab
  / Propagation Delay: $"Distanz"/"Geschwindigkeit"$ Zeit, die ein Bit von A nach B benötigt
  / Processing Delay: Zeit, um ein Paket zu verabeiten
  / Queuing Delay: Zeit, bis ein Paket gesendet werden kann
]

== Go-Back-N ARQ
#definition[Go-Back-N ARQ][
  Sender darf bis zu $N$ Parameter ohne Quittung senden. Quittungen sind
  kumulative Quittungen. Sendefenster $W$ gibt an, wieviele Daten ohne Quittung
  noch gesendet werden dürfen.
]
#image("res/Sendefenster bei Go-Back-N.png")
#todo[Leistungsbewertung][]

== Selective Repeat
#todo[][]
#todo[Ganz viel weiteres...][]

= Datenkommunikation
*(Kommunikations-)Verbindung* durch *(Kommunikations-)Verbindungskontext*
charakterisiert]\
Kontext: #zb Sequenznummer, Fenstergröße]\
*Aufbau* $->$ Datenübermittlung $->$ *Abbau*]\
_Aktueller Trend_ Verbindungsaufbau bereits mit Datenaustausch kombinieren, um
Latenzen zu senken]\

#image("res/Kommunikation Ablauf.png")

= Transportschicht (also TCP)
Transportprotokolle sind auf den Ensystemen lokalisiert

#concept[Ports (TCP)][
  / Well-known Ports: (0-1023) Vergabe durch Internet Assigned Numbers Authority (IANA).
    Systemprozesse und privilegierte Prozesse
  / Registered Ports: (1024-49151)Registrierung durch IANA. Nutzung durch Nutzerprozesse ohne
    besondere Rechte
  / Dynamic/private ports: (49152-65535). Keine Registrierung vorgesehen
]

#concept[Secure Shell (SSH)][
  _Sicherer Zugriff_ über _unsicheres Netz_
]
#definition[Datenstrom/Flow][
  5-Tupel (Ziel IP, Quell IP, Ziel Port, Quell Port, Transportprotokoll)
]

#fact[UDP][
  - Best Effor
  - Kleiner Kopf
  - Unreguliertes Senden
  - stateless & kontextlos
]

#fact[Transmission Control Protocol (TCP)][
  - *zuverlässig*
  - sendet Segmente
  - Segmentteilung des Bytestroms "at its own convenience" (/Maximumn Segment Size,
    Push flag im Kopf, Zeitgeber)
  - Fehlerkontrolle: Prüfsumme, Sequenznummern
  - Flusskontrolle: Empfänger nicht überlasten
  - Staukontrolle: Netz nicht überlasten
]
Sequenznummnern pro Byte, NICHT pro Segment\
Initiale Sequenznummer wählt Endsystem zufällig
#fact[Quittung][
  - positiv
  - kumulativ
  - Sequenznummer des nächsten Bytes
]
#fact("TCP Kopf", image("res/TCP Segmentkopf.png"))
/ Offset: \# der 32-Bit Wörter im TCP-Kopf
/ URG: Urgent Pointer (in der Regel nicht benutzt)
/ SYN: Beim Verbindungsaufbau, um Connection Request oder Connection Confirmation
       anzuzeigen
/ RST: Setzt Verbindung zurück
/ PSH: Daten sollen direkt weitergeleitet werden (in der Regel nicht benutzt)

#note[Flusskontrolle beim Empfänger][
  - LastByteRcvd
  - LastByteRead
  Es muss immer gelten:
  $
    "LastByteRcvd - LastByteRead <= RcvBuffer"
  $
  Empfangsfenster:
  $
    "RcvWindow" = "RcvBuffer" - ("LastByteRcvd" - "LastByteRead")
  $
]
#note[Flusskontrolle beim Sender][
  - LastByteSent
  - LastByteAcked
  Es muss immer gelten:
  $
    "LastByteSent" - "LastByteAcked" <= "RcvWindow"
  $
]

#fact[Verbindungsaufbau][3-Way-Handshake][
  Client an Sender\
  + TConReq $->$\
  + $<-$ TConCnf\
  + ACK $->$
]
#fact[Verbindungsabbau][4-Wege-Handshake][
  In beide Richtungen\
  + TDisReq $->$\
  + $<-$ TDisConf\
Es muss in beide Richtungen geschehen, damit beide Parteien die Chance haben,
verlorene Pakete nochmal anzufordern
]
Es gibt auch _Reset (RST-Segment)_, um eine 
Verbindung abzubrechen und unmittelbar zu schließen
#example([Typische Zustandsfolge], image("res/TCP States.png"))

#definition[Flusskontrolle vs. Staukontrolle][
_Vermeidung von Überlastsituationen im Netz_\
  Flusskontrolle, um Verlust von Packages sicherzustellen.\
  Staukontrolle hingegen, um Überlauf von Puffern und Failure von Zwischensystemen zu verhindern.
  -> Pakete werden bei Überlauf verworfen!
]

#definition[Congestion Window][CWnd][Staukontrollfenster][
  Beim Senden wird minimum aus Staukontrollfenster und Empfangsfenster berücksichtigt.
]
Stau wird zb. durch Timer erkannt (ausbleibende Quittung). Dann wird das CWnd wieder langsam an die Netzkapazität herangetastet.\
Staukontrolle ist derzeit ein aktuelles Thema im Internet.

= Dienste und Modelle
== Kommunikationsmodell
/ Nutzer: N Sender und N Empfänger
/ Abstraktes Medium: Kabel und co.

== Service Model
#definition[Dienst][Serice][
/ Dienstnehmer und Dienstbringer:
/ Service Access Point: Interface
/ Dienstprimitive: Beschreiben die Interaktionen zwischen Dienstnehmer und Access Point
]
#definition[Dienstprimitiven][
/ Request:
/ Indication: Benachrichtigung des Partners
/ Response:
/ Confirmation: Benachrichtigung über Abschluss
]
*Unzuverlässiger Dienst* (Best Effort) #vs *Zuverlässiger Dienst* #vs *Unbestätigter Dienst* (Keine Confirmation)

#definition[Zuverlässiger Dienst][
Daten sind
  - korrekt
  - vollständig und in der richtigen Reihenfolge
  - duplikatfrei
  - phantomfrei
]
#definition[
  / Protokollinstanz: Kommunikationspartner
]

== Schichten | Modellierung
// #let definition = definition.with[Modellierung]
5. #definition[Physikalische Schicht][
  Zwei physikalisch direkt verbundene Geräte können Bits austauschen.\
  Bietet Schicht 2 einen unzuverlässigen Dienst an.
]
4. #definition[Data Link Layer][Sicherungsschicht][
  - Stellt zuverlässige und unzuverlässige Dienste bereit
  - Adressierung der Geräte
  - Pufferung von Daten sowohl beim Sender, als auch Empfänger
  - Strukturierung in Frames
]
3. #definition[Network Layer][Vermittlungsschicht][
  - Da Systeme i.d.R. nicht benachbart sind werden Zwischensysteme benötigt
  - Macht Routing
  -> Pakete werden als Datagramme bezeichnet
]
2. #definition[Transportschicht][TCP und UDP]
1. #definition[Anwendungsschicht][
  Hier werden *Nachrichten* ausgetauscht
]

#concept[Nuzter-zu-Nutzer #vs Ende-zu-Ende][
  Transportschicht ist Nutzer-zu-Nutzer #vs Vermittlungsschicht ist Ende-zu-Ende
]

#fact[Datagramm Aufbau][Zusammenspiel der Schichten][
  #image("res/Datagram Aufbau.png")
]

== OSI Referenzmodell
#align(center, block[
#set enum(numbering: it => 8 - it)
#show enum: set align(left)
#show enum: set text(1.5em)
+ Anwendungsschicht
+ Darstellungsschicht
+ Sitzungsschicht
+ Transportschicht
+ Vermittlungsschicht
+ Sicherungsschicht
+ Physikalische Schicht
])

#include "5-vermittlungsschicht.typ"
#include "6-sicherungsschicht.typ"

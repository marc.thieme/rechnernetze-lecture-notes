#import "lib.typ" : *

= Sicherunsgsschicht
Aufgaben: 
- Pufferung
- Adressierung der Geräte
- Strukturierung der Übertragungen

#definition[Broadcast #vs Punkt-zu-Punkt][
/ Broadcast: Alle angeschlossenen Geräte können einen gesendeten Frame sehen
/ Punkt-zu-Punkt: Zwei Geräte sind direkt verbunden
/ > Simplex: Übertragung nur in eine Richtung
/ > Halbduplex: Übertragun in beide Richtungen, aber _abwechselnd_
/ > Duplex: Gleichzeitig in beide Richtungen möglich
]
Ist auf Network Interface Card implementiert. Kombination aus Hardware, Software, Firmware.

== Netztopologien
/ Vollvermascht:
/ Teilvermascht:
/ Hierarchische Topologie: Baum
/ Stern: Client-Server
/ Ring:
/ Bus: Alle sind an selbem Link

== Multiplexen
#fact[Multiplex Dimensionen][
  - Raum
  - Zeit
  - Frequenz
  - Code
]

#definition[Signal][
  Ein Signal ist eine mathematische Funktion von mindestens einer unabhängigen Variablen (der Zeit).
]

#definition[Signal Klassen][
#set align(center)
  #image("res/singal klassen.png", height: 30%)
]

#definition[Space Division Multiple Access (FDMA)][
  Jedes Signal bekommt ein exklusives Medium
]
#definition[Frequency Division Multiple Access (FDMA)][
  Bandbreite wird in Frequenzbänder und auf Signale aufgeteilt
]
#definition[Time Division Multiple Access (TDMA)][
  Signale verwenden das Medium abwechselnd
]
#definition[Zeitmultiplex][
  / Statisch: Off-line konfigueriert und ändert sich während des Betriebs nicht
  / Dynamisch:
  / > Fest: Während des Betriebs signalisiert
  / > Variablel: On-Deman Zugriff, #zb WLAN
]
#definition[Code Multiplex][
  Signal wird vom Sender mit Zufallszahl codiert, alle senden gleichzeitig auf gleichem Band
]

#concept[Aloha][
  Erstes Multiplex Protokoll.\
  Zugriff erfolgt variabel on-demand, kann Kollisionen geben\
  ACK auf separatem Kanal von höheren Ebenen benötigt, um Kollisionen zu erkennen.
]

#concept[Carrier Sense Multiple Access (CSMA)][
  Gerät prüft vor Senden, ob Medium frei ist.\
  -> Kollision, wenn 2 Geräte genau gleichzeitig mit Senden beginnen.
]
#concept[Carrier Sense Multiple Access mit Collision Detection (CSMA/CD)][
  Im Kollisionsfall Sende abbrechen
]
#concept[Wann wird gerät frei][
/ Persistenzen: warten bis Gerät frei wird und dann unmittelbar senden.
/ Nicht-Persistent: Nach zufälligem Intervall nochmal abfragen, ob jetzt frei.
/ > p-Persistenz: Warte zuföllige Zeiteinheit mit W.keit 1-p
/ > Exponentieller Backoff: Warte exponentiell viel länger
]
#concept[Token Passing][
  Sendericht hat der, der Token besitzt. Dieser muss herumgereicht werden.
  / Token Holding Time: Begrenzt max. Sendezeit
  / Piggybacking: ???

  Funktioniert gut in Ring-Topologie (Token-Ring)
]
#concept[Polling][
  Zentrales Gerät managed Sendeberechtigungen.
]

Die Sicherungsschicht besteht aus 2 Subschichten:
- #fact[LLC][Responsibilties][Logical Link Control][
Einheitliches Interface für alle Medien
  - Strukturierung der Übertragung
  - Sicherung dvor Übertragungsfehlern
  - Sicherung vor Datenverlust
  - Sicherung der Reihenfolge
  - Flusskontrolle
]
- #fact[Reponsiblities][Media Access Control][
Zugangskontrolle für geteiltes Medium
  - Random Acess
  - Collision free, controlled access
]

== MAC Adressen
24 Bit durch IEEE an Hersteller zugewiesen, 24 weitere Bit von Hersteller durchnummeriert

#fact[Ethernet Rahmen][
  / PR: Präambel zur Synchronisierung (101010101010...)
  / SD: Start-of-Frame Delimeter zeigt Beginn an (10101011)
  / DA: Destination Adress
  / SA: Source Adress
  / Type/Length: Darüber liegendes Protokoll & Länge der Nutzdaten
  / Data:
  / PAD: Padding
  / FCS: Frame checksu
]

#definition[Switching][
  Weiterleitung von Eingangsinterface an Ausgangsinterface
]

#definition[Kollisionsdomäne][
  Der Bereich des Netzes, der vom Auftreten einer Kollision betroffen ist.
]
#definition[Repeater][
  Koppelt _busbasierte_ Netzbereiche durch eine *Brücke*
]

== Address Resolution Protocol (ARP)
Resolve IP-Adress to MAC Adress
#fact([ARP Package], image("res/arp package.png", height: 25%))

#import "lecture-notes-template/lecture-notes.typ": *

#let (definition, example, concept, observe, note, fact, todo, notation, idea) = init-theorems(
  definition: "Definition",
  example: "Beispiel",
  concept: "Konzept",
  observe: "Beobachtung",
  note: "Notiz",
  fact: "Fakt",
  todo: "Todo",
  notation: "Notation",
  idea: "Idee"
)

#let ws22(..i) = [WS22: #(i.pos().map(str).join(","))]

